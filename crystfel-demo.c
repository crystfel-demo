/*
 * crystfel-demo.c
 *
 * CrystFEL demo program
 *
 * Copyright © 2019 Deutsches Elektronen-Synchrotron DESY,
 *                  a research centre of the Helmholtz Association.
 *
 * Authors:
 *   2019 Thomas White <taw@physics.org>
 *
 * This file is part of CrystFEL.
 *
 * CrystFEL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CrystFEL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CrystFEL.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <gtk/gtk.h>
#include <sys/stat.h>
#include <errno.h>


struct crystfeldemo
{
	GSubprocess *examine_raw_files;
	GSubprocess *check_peak_detection;
	GSubprocess *check_near_bragg;
};


gint stop_examine_raw(GtkWidget *widget, struct crystfeldemo *demo)
{
	if ( demo->examine_raw_files == NULL ) {
		printf("Not running!\n");
		return 0;
	}

	g_subprocess_send_signal(demo->examine_raw_files, SIGTERM);
	g_subprocess_wait(demo->examine_raw_files, NULL, NULL);
	demo->examine_raw_files = NULL;

	return 0;
}


gint examine_raw_files(GtkWidget *widget, struct crystfeldemo *demo)
{
	GError *error = NULL;

	if ( demo->examine_raw_files != NULL ) {
		printf("Already running!\n");
		return 0;
	}

	demo->examine_raw_files = g_subprocess_new(G_SUBPROCESS_FLAGS_NONE,
	           &error, "sh", "-c",
	           "${CRYSTFEL_DEMO_FILES}/random-image "
	           "${CRYSTFEL_DEMO_FILES}/files.lst",
	           NULL);

	if ( demo->examine_raw_files == NULL ) {
		printf("Failed to start demo process\n");
	}
	return 0;
}


gint stop_peak_detection(GtkWidget *widget, struct crystfeldemo *demo)
{
	if ( demo->check_peak_detection == NULL ) {
		printf("Not running!\n");
		return 0;
	}

	g_subprocess_send_signal(demo->check_peak_detection, SIGTERM);
	g_subprocess_wait(demo->check_peak_detection, NULL, NULL);
	demo->check_peak_detection = NULL;

	return 0;
}


gint check_peak_detection(GtkWidget *widget, struct crystfeldemo *demo)
{
	GError *error = NULL;

	if ( demo->check_peak_detection != NULL ) {
		printf("Already running!\n");
		return 0;
	}

	demo->check_peak_detection = g_subprocess_new(G_SUBPROCESS_FLAGS_NONE,
	           &error, "sh", "-c",
	           "${CRYSTFEL_DEMO_FILES}/check-peak-detection "
	           "${CRYSTFEL_DEMO_FILES}/peaks.stream "
	           "--geometry=${CRYSTFEL_DEMO_FILES}/5HT2B-Liu-2013.geom",
	           NULL);

	if ( demo->check_peak_detection == NULL ) {
		printf("Failed to start demo process\n");
	}
	return 0;
}


gint examine_geom_files(GtkWidget *widget, struct crystfeldemo *demo)
{
	GError *error = NULL;

	if ( demo->examine_raw_files != NULL ) {
		printf("Already running!\n");
		return 0;
	}

	demo->examine_raw_files = g_subprocess_new(G_SUBPROCESS_FLAGS_NONE,
	           &error, "sh", "-c",
	           "${CRYSTFEL_DEMO_FILES}/random-image "
	           "${CRYSTFEL_DEMO_FILES}/files.lst "
	           "--geometry=${CRYSTFEL_DEMO_FILES}/5HT2B-Liu-2013.geom",
	           NULL);

	if ( demo->examine_raw_files == NULL ) {
		printf("Failed to start demo process\n");
	}
	return 0;
}


gint show_stream(GtkWidget *widget, struct crystfeldemo *demo)
{
	GError *error = NULL;
	GSubprocess *sub;

	sub = g_subprocess_new(G_SUBPROCESS_FLAGS_NONE,
	                       &error, "xfce4-terminal",
	                       "-x", "sh", "-c",
	                       "less ${CRYSTFEL_DEMO_FILES}/nocell.stream",
	                       NULL);

	if ( sub == NULL ) {
		printf("Failed to start demo process\n");
	}
	return 0;
}


gint cell_explorer_nocell(GtkWidget *widget, struct crystfeldemo *demo)
{
	GError *error = NULL;
	GSubprocess *sub;

	sub = g_subprocess_new(G_SUBPROCESS_FLAGS_NONE,
	                       &error, "sh", "-c",
	                       "cell_explorer ${CRYSTFEL_DEMO_FILES}/nocell.stream",
	                       NULL);

	if ( sub == NULL ) {
		printf("Failed to start demo process\n");
	}
	return 0;
}


gint cell_explorer_cell(GtkWidget *widget, struct crystfeldemo *demo)
{
	GError *error = NULL;
	GSubprocess *sub;

	sub = g_subprocess_new(G_SUBPROCESS_FLAGS_NONE,
	                       &error, "sh", "-c",
	                       "cell_explorer ${CRYSTFEL_DEMO_FILES}/cell.stream",
	                       NULL);

	if ( sub == NULL ) {
		printf("Failed to start demo process\n");
	}
	return 0;
}


gint stop_near_bragg(GtkWidget *widget, struct crystfeldemo *demo)
{
	if ( demo->check_near_bragg == NULL ) {
		printf("Not running!\n");
		return 0;
	}

	g_subprocess_send_signal(demo->check_near_bragg, SIGTERM);
	g_subprocess_wait(demo->check_near_bragg, NULL, NULL);
	demo->check_near_bragg = NULL;

	return 0;
}


gint check_near_bragg(GtkWidget *widget, struct crystfeldemo *demo)
{
	GError *error = NULL;

	if ( demo->check_near_bragg != NULL ) {
		printf("Already running!\n");
		return 0;
	}

	demo->check_near_bragg = g_subprocess_new(G_SUBPROCESS_FLAGS_NONE,
	           &error, "sh", "-c",
	           "${CRYSTFEL_DEMO_FILES}/check-near-bragg "
	           "${CRYSTFEL_DEMO_FILES}/cell.stream "
	           "--geometry=${CRYSTFEL_DEMO_FILES}/5HT2B-Liu-2013.geom",
	           NULL);

	if ( demo->check_near_bragg == NULL ) {
		printf("Failed to start demo process\n");
	}
	return 0;
}


gint peakogram(GtkWidget *widget, struct crystfeldemo *demo)
{
	GError *error = NULL;
	GSubprocess *sub;

	sub = g_subprocess_new(G_SUBPROCESS_FLAGS_NONE,
	                       &error, "sh", "-c",
	                       "${CRYSTFEL_DEMO_FILES}/peakogram-stream -i "
	                       "${CRYSTFEL_DEMO_FILES}/cell.stream",
	                       NULL);

	if ( sub == NULL ) {
		printf("Failed to start demo process\n");
	}
	return 0;
}


gint detector_shift(GtkWidget *widget, struct crystfeldemo *demo)
{
	GError *error = NULL;
	GSubprocess *sub;

	sub = g_subprocess_new(G_SUBPROCESS_FLAGS_NONE,
	                       &error, "sh", "-c",
	                       "${CRYSTFEL_DEMO_FILES}/detector-shift "
	                       "${CRYSTFEL_DEMO_FILES}/cell.stream",
	                       NULL);

	if ( sub == NULL ) {
		printf("Failed to start demo process\n");
	}
	return 0;
}


gint calc_fom(GtkWidget *widget, struct crystfeldemo *demo)
{
	GError *error = NULL;
	GSubprocess *sub;

	sub = g_subprocess_new(G_SUBPROCESS_FLAGS_NONE,
	                       &error, "xfce4-terminal",
	                       "-H", "-x", "sh", "-c",
	                       "sh ${CRYSTFEL_DEMO_FILES}/calc-foms.sh",
	                       NULL);

	if ( sub == NULL ) {
		printf("Failed to start demo process\n");
	}
	return 0;
}


gint show_reflist(GtkWidget *widget, struct crystfeldemo *demo)
{
	GError *error = NULL;
	GSubprocess *sub;

	sub = g_subprocess_new(G_SUBPROCESS_FLAGS_NONE,
	                       &error, "xfce4-terminal",
	                       "-H", "-x", "sh", "-c",
	                       "less ${CRYSTFEL_DEMO_FILES}/merged.hkl",
	                       NULL);

	if ( sub == NULL ) {
		printf("Failed to start demo process\n");
	}
	return 0;
}


static int change_to_tempdir()
{
	char tmpdir[64];
	struct stat s;

	snprintf(tmpdir, 64, "crystfel_demo_dir.%i", getpid());

	if ( stat(tmpdir, &s) == -1 ) {

		int r;

		if ( errno != ENOENT ) {
			printf("Failed to stat temporary folder.\n");
			return 1;
		}

		r = mkdir(tmpdir, S_IRWXU);
		if ( r ) {
			printf("Failed to create temporary folder: %s\n",
			      strerror(errno));
			return 1;
		}

	}

	if ( chdir(tmpdir) ) {
		printf("Failed to chdir to temporary folder: %s\n",
		      strerror(errno));
		return 1;
	}

	return 0;
}


int main(int argc, char *argv[])
{
	GtkBuilder *builder;
	GtkWidget *window;
	struct crystfeldemo demo;

	if ( change_to_tempdir() ) return 1;

	demo.examine_raw_files = NULL;
	demo.check_peak_detection = NULL;

	gtk_init(&argc, &argv);
	builder = gtk_builder_new_from_resource("/de/desy/crystfel-demo/crystfel-demo.glade");
	window = GTK_WIDGET(gtk_builder_get_object(builder, "mainwindow"));

	gtk_builder_add_callback_symbol(builder, "examine_raw_files",
	                                G_CALLBACK(examine_raw_files));
	gtk_builder_add_callback_symbol(builder, "examine_geom_files",
	                                G_CALLBACK(examine_geom_files));
	gtk_builder_add_callback_symbol(builder, "stop_examine_raw",
	                                G_CALLBACK(stop_examine_raw));

	gtk_builder_add_callback_symbol(builder, "check_peak_detection",
	                                G_CALLBACK(check_peak_detection));
	gtk_builder_add_callback_symbol(builder, "stop_peak_detection",
	                                G_CALLBACK(stop_peak_detection));

	gtk_builder_add_callback_symbol(builder, "show_stream",
	                                G_CALLBACK(show_stream));
	gtk_builder_add_callback_symbol(builder, "cell_explorer_nocell",
	                                G_CALLBACK(cell_explorer_nocell));

	gtk_builder_add_callback_symbol(builder, "cell_explorer_cell",
	                                G_CALLBACK(cell_explorer_cell));
	gtk_builder_add_callback_symbol(builder, "check_near_bragg",
	                                G_CALLBACK(check_near_bragg));
	gtk_builder_add_callback_symbol(builder, "stop_near_bragg",
	                                G_CALLBACK(stop_near_bragg));

	gtk_builder_add_callback_symbol(builder, "peakogram",
	                                G_CALLBACK(peakogram));
	gtk_builder_add_callback_symbol(builder, "detector_shift",
	                                G_CALLBACK(detector_shift));

	gtk_builder_add_callback_symbol(builder, "calc_fom",
	                                G_CALLBACK(calc_fom));
	gtk_builder_add_callback_symbol(builder, "show_reflist",
	                                G_CALLBACK(show_reflist));

	gtk_builder_connect_signals(builder, &demo);
	gtk_widget_show_all(window);
	gtk_main();

	return 0;
}
