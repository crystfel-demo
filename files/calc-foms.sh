#!/bin/sh

module load maxwell miniconda3
conda activate /gpfs/cfel/cxi/scratch/user/twhite/conda/envs/crystfel_dev

set -x

check_hkl ${CRYSTFEL_DEMO_FILES}/merged.hkl --highres=3 -p ${CRYSTFEL_DEMO_FILES}/5HT2B.cell
cat shells.dat

compare_hkl ${CRYSTFEL_DEMO_FILES}/merged.hkl1 ${CRYSTFEL_DEMO_FILES}/merged.hkl2 --highres=3 -p ${CRYSTFEL_DEMO_FILES}/5HT2B.cell --fom=cc
cat shells.dat

compare_hkl ${CRYSTFEL_DEMO_FILES}/merged.hkl1 ${CRYSTFEL_DEMO_FILES}/merged.hkl2 --highres=3 -p ${CRYSTFEL_DEMO_FILES}/5HT2B.cell --fom=Rsplit
cat shells.dat
