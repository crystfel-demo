#!/bin/sh
#SBATCH --partition=maxwell,cfel
#SBATCH --time=24:00:00
#SBATCH --nodes=1
#SBATCH --job-name  5HT2B-all
#SBATCH --output    5HT2B-all-%N-%j.out
#SBATCH --error     5HT2B-all-%N-%j.err
#SBATCH --mail-type FAIL
#SBATCH --mail-user thomas.white@desy.de

#source /gpfs/cfel/cxi/common/cfelsoft-rh7/setup.sh
#module load crystallography/latest
#export PATH=/install/bin:$PATH

module load maxwell miniconda3
conda activate /gpfs/cfel/cxi/scratch/user/twhite/conda/envs/crystfel_dev

indexamajig -i files.lst -o cell.stream -j `nproc` -g 5HT2B-Liu-2013.geom --peaks=peakfinder8 --threshold=800 --min-pix-count=1 --min-snr=3 -p 5HT2B.cell --indexing=xgandalf,mosflm
